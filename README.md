# Unity Depth Mask

An experiment with Unity depth mask shader. You could use this shader to dig a hole on geometry in Unity in runtime.

More about the shader pls see [DepthMask](http://wiki.unity3d.com/index.php?title=DepthMask) on [UnityCommunity](http://wiki.unity3d.com/index.php/Main_Page)

![](Docs/depthmask.gif)